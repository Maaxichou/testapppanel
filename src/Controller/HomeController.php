<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('home/home.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/chronologie", name="chronologie")
     */
    public function chronologie()
    {
        return $this->render('chronologie/chronologie.html.twig', [
            'controller_name' => 'HomeController',
        ]);    }

    /**
     * @Route("/404", name="404")
     */
    public function error()
    {
        return $this->render('404Page/error404.html.twig', [
            'controller_name' => 'HomeController',
        ]);    }
}
